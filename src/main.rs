use std::cmp::Eq;
use std::collections::HashMap;
use std::hash::Hash;

fn main() {
    let mut a = vec![20, 3, 3, 4, 4, 4, 5];
    let b: Vec<usize> = vec![];
    let v_str = vec![
        "Asdf".to_string(),
        "zsdf".to_string(),
        "bullshit".to_string(),
    ];
    println!("Mean   : {}", mean(&a));
    println!("Median : {}", median(&mut a));
    println!("Mode   : {}", mode(&a).unwrap_or(0));
    println!("Mode   : {}", mode(&b).unwrap_or(0));
    println!("Largest: {}", largest(&v_str).unwrap_or("".to_string()));
    println!("Largest: {}", largest(&a).unwrap_or(0));
}

/// Gets mean out of a vector
fn mean(v: &[usize]) -> f64 {
    let mut s: usize = 0;
    for number in v {
        s += number;
    }
    s as f64 / v.len() as f64
}

/// Gets median out of a vector
fn median(v: &mut [usize]) -> usize {
    v.sort_by(|a, b| a.cmp(b));
    v[v.len() / 2]
}

/// Gets (first) most repeated value out of a vector
fn mode<T: Hash + Copy + Eq>(v: &[T]) -> Option<T> {
    if v.len() == 0 {
        return None;
    }
    let mut w: HashMap<T, usize> = HashMap::new();
    for &el in v {
        let count = w.entry(el).or_insert(0);
        *count += 1;
    }
    let mut res = v[0];
    let mut count = 0;
    for (&k, &v) in &w {
        if v > count {
            res = k;
            count = v;
        }
    }
    Some(res)
}

/// Gets largest value from vector
fn largest<T: PartialOrd + Clone>(v: &[T]) -> Option<T> {
    if v.len() == 0 {
        return None;
    }
    let mut large = v[0].clone();
    for item in v {
        if *item > large {
            large = item.clone();
        }
    }
    Some(large)
}
